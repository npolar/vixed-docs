���"      �docutils.nodes��document���)��}�(�	rawsource�� ��children�]�h �section���)��}�(hhh]�(h �title���)��}�(h�Data requests�h]�h �Text����Data requests�����}�(hh�parent�hhh�source�N�line�Nuba�
attributes�}�(�ids�]��classes�]��names�]��dupnames�]��backrefs�]�u�tagname�hhhhhh�>/media/DataLocal/Projects/Vixed/vixed-docs/source/requests.rst�hKubh �	paragraph���)��}�(h�aRequests provide information to the service on which processors to use and how often to run them.�h]�h�aRequests provide information to the service on which processors to use and how often to run them.�����}�(hh/hh-hhhNhNubah}�(h]�h!]�h#]�h%]�h']�uh)h+hh*hKhhhhubh
)��}�(hhh]�(h)��}�(h�Format�h]�h�Format�����}�(hh@hh>hhhNhNubah}�(h]�h!]�h#]�h%]�h']�uh)hhh;hhhh*hKubh,)��}�(h��The format of the request files is JSON, which is a machine readable text document. It's human readable as well, but please pay attention to the formatting.�h]�h��The format of the request files is JSON, which is a machine readable text document. It’s human readable as well, but please pay attention to the formatting.�����}�(hhNhhLhhhNhNubah}�(h]�h!]�h#]�h%]�h']�uh)h+hh*hK	hh;hhubh,)��}�(h�^`start`, `stop`, `interval`, `send_to`, `processor`, `processor_settings` are required fields.�h]�(h �title_reference���)��}�(h�`start`�h]�h�start�����}�(hhhh`ubah}�(h]�h!]�h#]�h%]�h']�uh)h^hhZubh�, �����}�(h�, �hhZhhhNhNubh_)��}�(h�`stop`�h]�h�stop�����}�(hhhhsubah}�(h]�h!]�h#]�h%]�h']�uh)h^hhZubh�, �����}�(h�, �hhZubh_)��}�(h�
`interval`�h]�h�interval�����}�(hhhh�ubah}�(h]�h!]�h#]�h%]�h']�uh)h^hhZubh�, �����}�(h�, �hhZubh_)��}�(h�	`send_to`�h]�h�send_to�����}�(hhhh�ubah}�(h]�h!]�h#]�h%]�h']�uh)h^hhZubh�, �����}�(h�, �hhZubh_)��}�(h�`processor`�h]�h�	processor�����}�(hhhh�ubah}�(h]�h!]�h#]�h%]�h']�uh)h^hhZubh�, �����}�(hhrhhZubh_)��}�(h�`processor_settings`�h]�h�processor_settings�����}�(hhhh�ubah}�(h]�h!]�h#]�h%]�h']�uh)h^hhZubh� are required fields.�����}�(h� are required fields.�hhZhhhNhNubeh}�(h]�h!]�h#]�h%]�h']�uh)h+hh*hKhh;hhubh,)��}�(h��Request file always have to contain `processor` field. The service must know which processor to use and will check the provided name in the list of registered processors.�h]�(h�$Request file always have to contain �����}�(h�$Request file always have to contain �hh�hhhNhNubh_)��}�(h�`processor`�h]�h�	processor�����}�(hhhh�ubah}�(h]�h!]�h#]�h%]�h']�uh)h^hh�ubh�{ field. The service must know which processor to use and will check the provided name in the list of registered processors.�����}�(h�{ field. The service must know which processor to use and will check the provided name in the list of registered processors.�hh�hhhNhNubeh}�(h]�h!]�h#]�h%]�h']�uh)h+hh*hKhh;hhubh,)��}�(h��`processor_settings` part of the document describes the parameters which are specific to the indicated processor. This can differ a lot from processor to processor.�h]�(h_)��}�(h�`processor_settings`�h]�h�processor_settings�����}�(hhhh�ubah}�(h]�h!]�h#]�h%]�h']�uh)h^hh�ubh�� part of the document describes the parameters which are specific to the indicated processor. This can differ a lot from processor to processor.�����}�(h�� part of the document describes the parameters which are specific to the indicated processor. This can differ a lot from processor to processor.�hh�hhhNhNubeh}�(h]�h!]�h#]�h%]�h']�uh)h+hh*hKhh;hhubh,)��}�(h�Below is a sample request file:�h]�h�Below is a sample request file:�����}�(hj  hj  hhhNhNubah}�(h]�h!]�h#]�h%]�h']�uh)h+hh*hKhh;hhubh �literal_block���)��}�(hX�  {
 "start":
        ["2018-05-02T08:23:00Z",
         "2018-05-02T08:24:00Z"],
 "stop":"2018-05-21T00:00:00Z",
 "interval": "10m",
 "send_to": ["user@example.com"],
 "processor": "ping",
 "processor_settings": {
        "time_window": ["timestamp1","timestamp2"],
        "roi": {
                        "coordinates": [
                                [[37, 82],
                                 [20, 81],
                                 [15, 77],
                                 [-23, 76],
                                 [-37, 82]]
                                ],
                        "type": "Polygon"
                },
        "spatial_resolution": 100,
        "crs": "EPSG:3575"
        }
}�h]�hX�  {
 "start":
        ["2018-05-02T08:23:00Z",
         "2018-05-02T08:24:00Z"],
 "stop":"2018-05-21T00:00:00Z",
 "interval": "10m",
 "send_to": ["user@example.com"],
 "processor": "ping",
 "processor_settings": {
        "time_window": ["timestamp1","timestamp2"],
        "roi": {
                        "coordinates": [
                                [[37, 82],
                                 [20, 81],
                                 [15, 77],
                                 [-23, 76],
                                 [-37, 82]]
                                ],
                        "type": "Polygon"
                },
        "spatial_resolution": 100,
        "crs": "EPSG:3575"
        }
}�����}�(hhhj&  ubah}�(h]�h!]�h#]�h%]�h']��	xml:space��preserve��language��json��linenos���highlight_args�}�uh)j$  hh*hKhh;hhubh �comment���)��}�(hhh]�h}�(h]�h!]�h#]�h%]�h']�j4  j5  uh)j;  hh;hhhh*hK/ubh
)��}�(hhh]�(h)��}�(h�Submitting requests via mail�h]�h�Submitting requests via mail�����}�(hjK  hjI  hhhNhNubah}�(h]�h!]�h#]�h%]�h']�uh)hhjF  hhhh*hK3ubh,)��}�(h��To submit request simply attache the `request.json` file and send to the service email address. Currently Vixed-d is set to work with the gmail mail boxes.�h]�(h�%To submit request simply attache the �����}�(h�%To submit request simply attache the �hjW  hhhNhNubh_)��}�(h�`request.json`�h]�h�request.json�����}�(hhhj`  ubah}�(h]�h!]�h#]�h%]�h']�uh)h^hjW  ubh�h file and send to the service email address. Currently Vixed-d is set to work with the gmail mail boxes.�����}�(h�h file and send to the service email address. Currently Vixed-d is set to work with the gmail mail boxes.�hjW  hhhNhNubeh}�(h]�h!]�h#]�h%]�h']�uh)h+hh*hK5hjF  hhubeh}�(h]��submitting-requests-via-mail�ah!]�h#]��submitting requests via mail�ah%]�h']�uh)h	hh;hhhh*hK3ubh
)��}�(hhh]�(h)��}�(h�Submitting HTTP requests�h]�h�Submitting HTTP requests�����}�(hj�  hj�  hhhNhNubah}�(h]�h!]�h#]�h%]�h']�uh)hhj�  hhhh*hK9ubh,)��}�(h�TBD�h]�h�TBD�����}�(hj�  hj�  hhhNhNubah}�(h]�h!]�h#]�h%]�h']�uh)h+hh*hK;hj�  hhubeh}�(h]��submitting-http-requests�ah!]�h#]��submitting http requests�ah%]�h']�uh)h	hh;hhhh*hK9ubeh}�(h]��format�ah!]�h#]��format�ah%]�h']�uh)h	hhhhhh*hKubeh}�(h]��data-requests�ah!]�h#]��data requests�ah%]�h']�uh)h	hhhhhh*hKubah}�(h]�h!]�h#]�h%]�h']��source�h*uh)h�current_source�N�current_line�N�settings��docutils.frontend��Values���)��}�(hN�	generator�N�	datestamp�N�source_link�N�
source_url�N�toc_backlinks��entry��footnote_backlinks�K�sectnum_xform�K�strip_comments�N�strip_elements_with_classes�N�strip_classes�N�report_level�K�
halt_level�K�exit_status_level�K�debug�N�warning_stream�N�	traceback���input_encoding��	utf-8-sig��input_encoding_error_handler��strict��output_encoding��utf-8��output_encoding_error_handler�j�  �error_encoding��UTF-8��error_encoding_error_handler��backslashreplace��language_code��en��record_dependencies�N�config�N�	id_prefix�h�auto_id_prefix��id��dump_settings�N�dump_internals�N�dump_transforms�N�dump_pseudo_xml�N�expose_internals�N�strict_visitor�N�_disable_config�N�_source�h*�_destination�N�_config_files�]��pep_references�N�pep_base_url�� https://www.python.org/dev/peps/��pep_file_url_template��pep-%04d��rfc_references�N�rfc_base_url��https://tools.ietf.org/html/��	tab_width�K�trim_footnote_reference_space���file_insertion_enabled���raw_enabled�K�syntax_highlight��long��smart_quotes���smartquotes_locales�N�character_level_inline_markup���doctitle_xform���docinfo_xform�K�sectsubtitle_xform���embed_stylesheet���cloak_email_addresses���env�N�gettext_compact��ub�reporter�N�indirect_targets�]��substitution_defs�}��substitution_names�}��refnames�}��refids�}��nameids�}�(j�  j�  j�  j�  j~  j{  j�  j�  u�	nametypes�}�(j�  Nj�  Nj~  Nj�  Nuh}�(j�  hj�  h;j{  jF  j�  j�  u�footnote_refs�}��citation_refs�}��autofootnotes�]��autofootnote_refs�]��symbol_footnotes�]��symbol_footnote_refs�]��	footnotes�]��	citations�]��autofootnote_start�K�symbol_footnote_start�K �id_start�K�parse_messages�]��transform_messages�]��transformer�N�
decoration�Nhhub.