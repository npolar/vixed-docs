Data request files
==================

Requests provide information to the service on which processors to use and how often to run them.

Format
******

The format of the request files is JSON, which is a machine readable text document. It's human readable as well, but please pay attention to the formatting.

`start`, `stop`, `interval`, `send_to`, `processor`, `processor_settings` are required fields.

Request file always have to contain `processor` field. The service must know which processor to use and will check the provided name in the list of registered processors.

`processor_settings` part of the document describes the parameters which are specific to the indicated processor. This can differ a lot from processor to processor.

Below is a sample request file:

.. code-block:: json


	{
	 "start":
	 	["2018-05-02T08:23:00Z",
	 	 "2018-05-02T08:24:00Z"],    
	 "stop":"2018-05-21T00:00:00Z", 
	 "interval": "10m",
	 "send_to": ["user@example.com"],
	 "processor": "ping",
	 "processor_settings": {
	 	"time_window": ["timestamp1","timestamp2"], 
	 	"roi": { 
	 			"coordinates": [
	 				[[37, 82],
	 				 [20, 81], 
	 				 [15, 77],
	 				 [-23, 76],
	 				 [-37, 82]]
	 				],
	 			"type": "Polygon"
	 		},
	 	"spatial_resolution": 100,
	 	"crs": "EPSG:3575"
	 	}
	}

..


Submitting requests via mail
----------------------------

To submit request simply attach the `request.json` file and send to the service email address. Currently Vixed-d is set to work with the gmail mail boxes.


Submitting HTTP requests
------------------------

TBD

Examples
========

Below are examples of some useful processor requests. These can be put in a text file with ´.json´ file extension and send to service email box.
NB: if "run_true" key is set to True, service will ignore the start and stop keys and will perform the job only once.

SAR processor
-------------

Request for arbitrary polygon with 4 vertices, 500 meters spatial resolution and search window of 48 hours

```
{
	"interval": "24h",
	"processor": "SAR", 
	"processor_settings": 
	{
		"compress": "True",
		"crs": "epsg:3857",
		"platformname": "Sentinel-1",
		"producttype": "GRD",
		"roi":
			{"coordinates":
				[[[0, 80], [10, 80], [10, 81], [0, 81], [0, 80]]],
			"type": "Polygon"},

	"sensoroperationalmode": "EW",
	"spatial_resolution": 500,
	"time_delta_hours": 48}, 
	"send_to": ["user@example.com"], 
	"start": ["1900-05-21T20:00:00Z"],
	"stop": "1900-05-23T19:01:00Z",
	"run_now": true
		
}
```

Request for satellite data defined by the square box (center and radius). Box size is given in meters, not km!
```
{
	"interval": "48h",
    "processor": "SAR",
    "processor_settings":
	{
		"compress": "True",
		"crs": "EPSG:3395",
		"platformname": "Sentinel-1",
		"producttype": "GRD",
		"roi": "bbox",
		"box_radius": 100000,
		"center_lat": 82.50,
		"center_lon": 10.00,
		"sensoroperationalmode": "EW",
		"spatial_resolution": 500,
		"time_delta_hours": 48
    },
    "send_to": [
        user@example.com"	
    ],
    "start": [
        "2019-07-29T19:11:00Z"
    ],
    "stop": "2019-08-15T23:00:00Z",
	"run_now": true
}
```
MET forecast
------------
