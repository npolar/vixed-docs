Quickstart
------------

Vixed stands for `VersatIle eXternal processor SchEDuler`

Vixed is a multicomponent server side software. It generates geophysical products on the remote servers and sends out the products to the users.

Vixed is designed to be as simple as possible. It is modular and its functionality is meant to be easily extended to add new products. In fact, any types of files can be sent.

Interaction with Vixed
======================

.. mermaid::

    sequenceDiagram 
        Captain->>Captain: Create/re-use request file
        Captain->>MailServer: Send custom data request via email
        VixedD->>MailServer: Check for new requests
        VixedD->>Processor: Launch relevant processor
        Processor->>SatelliteCloud: Check for new data
        SatelliteCloud-->>Processor: Serve data
        Processor->>Processor: Process data
        VixedD->>Processor: Pick processing output and logs
        VixedD->>MailServer: Send map products as email
        MailServer->>Captain: Forward the map products

..

|

Getting started
===============

* Add yourself to the users whitelist (ask the data section to do that)
* Create a request file (optionally: validate the file before sending) 
* Send request file as attachment to the service mail address
* Start receiving data (or error logs)

