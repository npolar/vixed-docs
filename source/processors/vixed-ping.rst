ping
----

`ping` is the default test processor. It's meant to check if the service is functioning. It is part of the vixed daemon service and therefore works without installing extra processors.

Default request to vixed for this processor would look like:

::

	{
		"start":["1900-01-01T08:23:00Z",
				 "1900-01-01T08:24:00Z"],
		"stop":"1901-01-01T00:00:00Z",
		"interval": "1d",
		"send_to": ["user@example.com"],
		"processor": "ping",
		"processor_settings": {},
	}


`ping` requires only empty dictionary for processor settings.
If ran successfully the output will look like:

::

    OUTPUT:

    Fri Nov 9 16:12:00 UTC 2018 : PONG!


    LOG:

    Fri Nov 9 16:12:00 UTC 2018 : Starting processing
    Fri Nov 9 16:12:00 UTC 2018 : Executed processor /opt/processor-ping/vixed-ping.sh successfully
