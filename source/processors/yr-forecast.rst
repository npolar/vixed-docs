yr-forecast
-----------

This processor will serve 5 days forecast from MET Norway for custom locations. Processor `yr-forecast` is currently the only processor that will work with simple text in the email body. It requires no subject and the only requirement is to write the coordinates in the following format:

::

  lat: 69; lon: 8

Note that the coordinates should be given in decimal format and separated by semicolon. Only one coordinate pair is accepted.
The output will be sent as a text message to senders address. See below for the output example:

::

	Results and log output below:

	OUTPUT:

	---
	Weather forecast by MET Norway, delivered by NPI
	Lat: 69.0000, Lon: 8.0000, Elev: 0
	---

	Date: 2018-11-09T16:00:00Z
			Temperature: 8.1 deg C, Pressure: 1017.6 HPa, Wind: 5.9 m/s S

	Date: 2018-11-09T19:00:00Z
			Temperature: 8.2 deg C, Pressure: 1018.1 HPa, Wind: 5.2 m/s S

	Date: 2018-11-09T22:00:00Z
			Temperature: 8.3 deg C, Pressure: 1017.8 HPa, Wind: 5.3 m/s S

	Date: 2018-11-10T01:00:00Z
			Temperature: 8.1 deg C, Pressure: 1017.9 HPa, Wind: 7.5 m/s S

	Date: 2018-11-10T04:00:00Z
			Temperature: 7.9 deg C, Pressure: 1016.5 HPa, Wind: 5.0 m/s SE

	Date: 2018-11-10T07:00:00Z
			Temperature: 8.1 deg C, Pressure: 1015.5 HPa, Wind: 5.8 m/s E

	Date: 2018-11-10T10:00:00Z
			Temperature: 7.9 deg C, Pressure: 1014.1 HPa, Wind: 8.1 m/s E

	Date: 2018-11-10T13:00:00Z
			Temperature: 7.9 deg C, Pressure: 1012.0 HPa, Wind: 9.7 m/s E

	Date: 2018-11-10T16:00:00Z
			Temperature: 8.1 deg C, Pressure: 1010.5 HPa, Wind: 8.5 m/s E

	Date: 2018-11-10T19:00:00Z
			Temperature: 8.2 deg C, Pressure: 1009.6 HPa, Wind: 10.2 m/s SE

	Date: 2018-11-10T22:00:00Z
			Temperature: 8.4 deg C, Pressure: 1008.9 HPa, Wind: 10.5 m/s SE

	Date: 2018-11-11T01:00:00Z
			Temperature: 8.4 deg C, Pressure: 1008.0 HPa, Wind: 10.7 m/s SE

	Date: 2018-11-11T04:00:00Z
			Temperature: 8.4 deg C, Pressure: 1007.0 HPa, Wind: 11.4 m/s SE

	Date: 2018-11-11T07:00:00Z
			Temperature: 8.4 deg C, Pressure: 1006.8 HPa, Wind: 10.3 m/s SE

	Date: 2018-11-11T10:00:00Z
			Temperature: 8.4 deg C, Pressure: 1007.0 HPa, Wind: 8.7 m/s SE

	Date: 2018-11-11T13:00:00Z
			Temperature: 8.3 deg C, Pressure: 1007.3 HPa, Wind: 7.1 m/s S

	Date: 2018-11-11T16:00:00Z
			Temperature: 8.2 deg C, Pressure: 1007.3 HPa, Wind: 4.6 m/s S

	Date: 2018-11-11T19:00:00Z
			Temperature: 8.1 deg C, Pressure: 1007.5 HPa, Wind: 2.8 m/s S

	Date: 2018-11-11T22:00:00Z
			Temperature: 7.9 deg C, Pressure: 1008.0 HPa, Wind: 2.1 m/s SW

	Date: 2018-11-12T01:00:00Z
			Temperature: 6.3 deg C, Pressure: 1007.7 HPa, Wind: 2.5 m/s SE

	Date: 2018-11-12T04:00:00Z
			Temperature: 7.1 deg C, Pressure: 1007.0 HPa, Wind: 3.0 m/s E

	Date: 2018-11-12T12:00:00Z
			Temperature: 8.0 deg C, Pressure: 1005.2 HPa, Wind: 5.4 m/s S

	Date: 2018-11-13T06:00:00Z
			Temperature: 7.4 deg C, Pressure: 1002.1 HPa, Wind: 6.0 m/s NE

	Date: 2018-11-14T00:00:00Z
			Temperature: 7.5 deg C, Pressure: 1004.2 HPa, Wind: 5.5 m/s SW



	LOG:

	2018-11-09 15:17:51,165  yrforecast  INFO: Retrieved forecast successfully

The `yr-forecast` processor relies on `MET Norway weather data API <https://api.met.no/>`_ for obtaining forecast data and `Mapbox Elevation API <https://www.mapbox.com/help/access-elevation-data/>`_ for obtaining point elevation. 
