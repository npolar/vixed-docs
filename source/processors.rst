Processors
==========
To allow processing of any kind of earth observation data the vixed daemon should be able to invoke external processors,
handle processors exit codes and access produced output. Processor is an executable that can be used as a standalone program and accepts optional arguments as described below.

The minimum set of cli arguments should be at least::

  --input_request_file <json-file>
  --output_file <file>
  --log_file <file>


Vixed will run the executable with provided JSON request file, pick up produced file and log contents and send them to the recipients indicated in the request file.

Existing processors
-------------------
.. toctree::
   :glob:
   :maxdepth: 2

   processors/*

Writing a new processor
-----------------------

Write an executable that accepts optional command line parameters like described above. The executable needs to be deployed into a custom location on the server and registered in the `/etc/vixed-d/config.json` file on the server.
